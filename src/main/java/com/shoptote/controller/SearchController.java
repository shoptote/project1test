package com.shoptote.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;





@CrossOrigin
@RestController
@RequestMapping("/search")

public class SearchController {
	
	
//	this is using 3rd API for a list of country ***************************************
	@Autowired 
	private RestTemplate restTemplate;
	private static String url = "https://restcountries.eu/rest/v2/all";
	
	@GetMapping("/countries")
	public List<Object> getCountries(){
		Object[] countries = restTemplate.getForObject(url, Object[].class);
	
		return Arrays.asList(countries);
		
	}
	
//	above *******************************************************************************
	
	@GetMapping("/nutrients")
	public String searchByNutrients() {
		return "please use req.body with post request";
	}
	
//	@PostMapping("/nutrients")
//	public String searchByNutrients(@RequestBody Paper Model) {
//		return "please enter nurtrients";
//	}
	
	@GetMapping("/ingredients")
	public String searchByIngredients() {
		return "please use req.body with post request for ingredients";
	}

	@GetMapping("/recipes")
	public String searchByRecipes() {
		return "please use req.body, in docs theres alot parameters";
	}
	
	@GetMapping("/id")
	public String searchByName() {
		return "please use req.body, should just use Id to search ";
	}
	
	
}
