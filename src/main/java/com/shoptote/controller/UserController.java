package com.shoptote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shoptote.model.Users;
import com.shoptote.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/user")


public class UserController {

	
	@Autowired
	private UserService us;
	
	@PostMapping("/register")
	public Users userRegister(@RequestBody Users users) {
		System.out.println(users +"from controller as well");
		us.userRegistration(users);
		return users;
	}
	
	@GetMapping("/favorite")
	public String searchFavorite() {
		return us.findAll();
	}
	
	
	
}
