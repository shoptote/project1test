package com.shoptote.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shoptote.model.Users;


public interface UsersRepository extends JpaRepository<Users, Integer>{

}
