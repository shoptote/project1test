package com.shoptote.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoptote.model.Users;
import com.shoptote.repository.UsersRepository;



@Service
public class UserService {
	
	@Autowired
	private UsersRepository ur;

	public void userRegistration(Users users) {
		System.out.println("registered users from service");
		ur.save(users);
	}
	
	
	public String findAll(){
		System.out.println("inside findAll from service");
		return "connected to repository";
	}
}
